import { MouseEvent } from '@agm/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService, ImageService } from '../_services';

class ImageSnippet {
  pending = false;
  status = 'init';

  constructor(public src: string, public file: File) {}
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: 'hs-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css']
})
export class DonateComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private imageService: ImageService
  ) {}

  // convenience getter for easy access to form fields
  get f() {
    return this.donateForm.controls;
  }
  donateForm: FormGroup;
  loading = false;
  submitted = false;
  selectedFile: ImageSnippet;
  position: Position;

  // google maps zoom level
  zoom = 12;
  lat = 13.068486;
  lng = 77.574853;

  // initial center position for the map
  marker: marker = {
    lat: 13.068486,
    lng: 77.574853,
    label: 'A',
    draggable: true
  };

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    this.donateForm.controls.marker.setValue($event.coords);
  }

  ngOnInit() {
    this.donateForm = this.formBuilder.group({
      name: ['Marzil', Validators.required],
      phoneNumber: [9243627255, [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      containersAvailable: [false, Validators.required],
      cutleryAvailable: [false, Validators.required],
      foodAvailableCount: [50, Validators.required],
      foodDescription: ['A lot of Rice', Validators.required],
      image: ['', Validators.required],
      marker: [this.marker, Validators.required]
    });
  }

  private onSuccess() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
  }

  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.selectedFile.pending = true;
      this.imageService.uploadImage(this.selectedFile.file).subscribe(
        res => {
          this.onSuccess();
        },
        err => {
          this.onError();
        }
      );
    });

    reader.readAsDataURL(file);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.donateForm.value);
  }
}
